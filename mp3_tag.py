#!/usr/bin/python3
#
"""

Autor: Joerg Sorge

Distributed under the terms of GNU GPL version 2 or later
Copyright (C) Joerg Sorge joergsorge at ggooogl
2017-07-18 ported to python3 2020-11-25

This script is a so called "nautilus script".
This script writes ID3-Tags in mp3 files.

Warning!!
This script removes present ID3-Tags and add only this new ones:
* TPE1
* TIT2
and if checked:
* TIT3

Depends on:
    python3, python3-tk, python3-mutagen
Install additional packages with:
    sudo apt-get install python3-tk python3-mutagen

"""

import os
import sys
import subprocess

try:
    import tkinter as tk
except ImportError:
    print("ImportError Tkinter")
    try:
        message = ("ImportError Tkinter!\nPlease install it with:\n"
                   + "sudo apt install python3-tk")
        subprocess.Popen(["zenity", "--info", "--text", message],
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE).communicate()
    except Exception as e:
        print(e.args[0])
    sys.exit()

from tkinter import (Frame, Label, Entry, Text, Button, Checkbutton,
                     scrolledtext, messagebox, LEFT, END, IntVar)

try:
    import mutagen
except ImportError:
    print("ImportError mutagen")
    try:
        message = ("ImportError mutagen!\nPlease install it with:\n"
                   + "sudo apt install python3-mutagen")
        subprocess.Popen(["zenity", "--info", "--text", message],
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE).communicate()
    except Exception as e:
        print (e.args[0])
    sys.exit()

from mutagen.id3 import ID3, TPE1, TIT2, TIT3
from mutagen.id3 import ID3NoHeaderError


class App(Frame):
    def __init__(self, master=None):
        """init of App"""
        self.author = ""
        self.title = ""
        self.descr = ""
        self.mp3_file = ""

        Frame.__init__(self, master)
        self.master.title("Simple mp3 Tagger")
        self.pack()

        self.text_box = scrolledtext.ScrolledText(self, height=8, width=100)

        self.mp3_file = self.check_file()

        if self.mp3_file is None:
            self.text_box.insert(END, "Please select a mp3 file!" + "\n")
            self.text_box.pack()
            return

        self.author, self.title, self.descr = self.load_id3_tags()

        self.frame_dummy = Frame(self)
        self.frame_dummy.pack(padx=5, pady=5)

        self.frame_a = Frame(self)
        self.label_a = Label(self.frame_a, width=10, text="Author")
        self.label_a.pack(side=LEFT)

        self.entry_a = Entry(self.frame_a, width=90)
        self.entry_a.pack(side=LEFT)
        self.entry_a.insert(0, self.author)
        self.frame_a.pack(padx=5, pady=5)

        self.frame_b = Frame(self)
        self.label_b = Label(self.frame_b, width=10, text="Title")
        self.label_b.pack(side=LEFT)

        self.entry_b = Entry(self.frame_b, width=90)
        self.entry_b.pack()
        self.entry_b.insert(0, self.title)
        self.frame_b.pack(padx=5, pady=5)

        self.frame_c = Frame(self)
        self.label_c = Label(self.frame_c, width=10, text="Description")
        self.label_c.pack(side=LEFT)

        self.entry_c = Text(self.frame_c, height=4, width=90)
        self.entry_c.pack()
        self.entry_c.insert("1.0", self.descr)
        self.frame_c.pack(padx=5, pady=5)

        self.check_value = IntVar()

        self.frame_d = Frame(self)
        self.check = Checkbutton(
            self.frame_d, text="Add Description Tag",
            variable=self.check_value)
        self.check.pack()

        self.frame_d.pack(padx=5, pady=5)
        self.text_box.pack()

        self.text_box.insert(
            END,
            "\n" + "If you need a description tag, "
            + "check the Add Description Tag!" + "\n")

        self.frame_d = Frame(self)
        self.button = Button(
            self.frame_d,
            text="Write ID3 Tags",
            command=self.write_id3_tags)
        self.button.pack()
        self.frame_d.pack(padx=5, pady=15)

    def check_file(self):
        """check if one mp3 file is selected"""
        print("check_file")

        # nautilus collects a list of selected files
        path_files = []
        try:
            path_files = (
                os.environ['NAUTILUS_SCRIPT_SELECTED_FILE_PATHS'].splitlines())
        except Exception as e:
            print(e.args[0])
            self.text_box.insert(END, "Seems, that's no file selected" + "\n")
            return None

        if len(path_files) > 1:
            self.text_box.insert(END, "More than one file selected" + "\n")
            return None
        else:
            self.text_box.insert(END, path_files[0] + "\n")

        if path_files[0].rfind(".mp3") == -1:
            self.text_box.insert(END, "It's not a mp3 file" + "\n")
            return None
        return path_files[0]

    def load_id3_tags(self):
        """load id3"""
        print("load id3")

        author = "no author"
        title = "no title"
        descr = "no deskription"

        try:
            mp3_meta = ID3(self.mp3_file)
        except ID3NoHeaderError:
            self.text_box.insert(END, "No Tags found" + "\n")
            return author, title, descr

        tags = mp3_meta.pprint().splitlines()

        for tag in tags:
            if tag[:4] == "TPE1":
                author = tag[5:]
                self.text_box.insert(END, "Author: " + author + "\n")
            if tag[:4] == "TIT2":
                title = tag[5:]
                self.text_box.insert(END, "Title: " + title + "\n")
            if tag[:4] == "TIT3":
                descr = tag[5:]
                self.text_box.insert(END, "Deskription: " + descr + "\n")

        return author, title, descr

    def write_id3_tags(self):
        """write_id3_tags"""
        print("write_id3_tags")

        result = messagebox.askquestion(
            "Tags", "Write ID3 Tags?", icon='warning')

        if result == 'no':
            self.text_box.insert(END, "Canceled" + "\n")
            self.text_box.see(END)
            return
        else:
            self.text_box.insert(END, "Writing.." + "\n")

        self.author = self.entry_a.get()
        self.title = self.entry_b.get()
        # get desc from position one to the last charakter and remove linebreak
        self.descr = self.entry_c.get("1.0", END + "-1c")

        self.text_box.insert(END, self.author + "\n")
        self.text_box.insert(END, self.title + "\n")
        self.text_box.insert(END, self.descr + "\n")

        try:
            mp3_meta = ID3(self.mp3_file)
            mp3_meta.delete()
            mp3_meta.add(TPE1(encoding=3, text=self.author))
            mp3_meta.add(TIT2(encoding=3, text=self.title))

            if self.check_value.get():
                self.text_box.insert(END, "Add deskription tag" + "\n")
                mp3_meta.add(TIT3(encoding=3, text=self.descr))
            else:
                self.text_box.insert(END, "No description tag added\n")

            mp3_meta.save()
            self.text_box.insert(END, "Tags written" + "\n")
        except ID3NoHeaderError:
            self.text_box.insert(END, "No tag present..." + "\n")
            try:
                mp3_meta = mutagen.File(self.mp3_file, easy=True)
                mp3_meta.add_tags()
                mp3_meta.save()

                try:
                    mp3_meta = ID3(self.mp3_file)
                    mp3_meta.add(TPE1(encoding=3, text=self.author))
                    mp3_meta.add(TIT2(encoding=3, text=self.title))
                    if self.check_value.get():
                        self.text_box.insert(END, "Add deskription tag" + "\n")
                        mp3_meta.add(TIT3(encoding=3, text=self.descr))
                    else:
                        self.text_box.insert(END, "No description tag added\n")

                    mp3_meta.save()
                    self.text_box.insert(END, "Tags written" + "\n")
                except Exception as e:
                    print(e.args[0])
                    self.text_box.insert(END, e.args[0])
            except Exception as e:
                print(e.args[0])
                self.text_box.insert(END, e.args[0])
        self.text_box.see(END)

def main():
    root = tk.Tk()
    root.minsize(840, 290)
    app = App(root)
    root.mainloop()


if __name__ == '__main__':
    main()
