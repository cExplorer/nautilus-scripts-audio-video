#!/bin/bash

# Dieses kleine Script uebernimmt die Installation von nautilus-scripts
# für ubuntu 20.4
#
# Aufruf in dem Verzeichnis, in dem das zu installierende Script liegt
#
# This script install nautilus-scripts from the current directory for
# ubuntu 20.04!!
#
# Author: Joerg Sorge
# Distributed under the terms of GNU GPL version 2 or later
# Copyright (C) mail@joergsorge.de
# 2021-02-17

echo ""
echo "Install Nautilus-Scripts local..."
echo "You may this script running at least on ubuntu 20.04!!!"
echo "It will add the ppa:flexiondotorg/audio"
echo "But only, if not exist:"
echo "/etc/apt/sources.list.d/flexiondotorg-ubuntu-audio-focal.list"
echo ""
read -p "Are you shure to proceed? (y/n) " -n 1
echo ""
if [[ ! $REPLY =~ ^[Yy]$ ]]; then
	echo ""
	echo "Installation aborted"
	exit
fi

for file in $PWD/*
do
   :
    if [ $(basename "$file") = "install_u_18_local.sh" ]
	    then
		    echo "skip $(basename "$file")"
		    continue
    fi
	if [ $(basename "$file") = "install_u_20_4_local.sh" ]
		then
            echo "skip $(basename "$file")"
            continue
	fi
	if [ $(basename "$file") = "update_nautilus_scripts.sh" ]
		then
            echo "skip $(basename "$file")"
            continue
	fi
	if [ $(basename "$file") = "README.md" ]
        then
            echo "skip $(basename "$file")"
            continue
	fi
    if [ $(basename "$file") = "LICENSE" ]
        then
            echo "skip $(basename "$file")"
            continue
	fi

	filename=$(basename "$file")
    # keep off extentions
    # Replace undelines with spaces for better reading nautilus menu
    filename=$(echo "$filename" | sed -r 's/[_]+/ /g')
    echo "copy $(basename "$file") to $filename"
	cp $file "/home/$USER/.local/share/nautilus/scripts/$filename"
	chmod u+x "/home/$USER/.local/share/nautilus/scripts/$filename"

done

echo ""
echo "PPA for mp3gain..."
echo "Check if it's alraedy there..."
echo ""

apt_source_file=/etc/apt/sources.list.d/flexiondotorg-ubuntu-audio-focal.list

if [ -f "$apt_source_file" ]; then
	echo "$apt_source_file exists, nothing to do..."
else
	sudo add-apt-repository ppa:flexiondotorg/audio
	sudo sed -i s/focal/bionic/g /etc/apt/sources.list.d/flexiondotorg-ubuntu-audio-focal.list
	sudo apt-get update
fi

echo ""
echo "Install libs..."
echo ""

sudo apt install \
lame mp3val libid3-tools mp3gain mp3info sox ffmpeg libsox-fmt-mp3 \
mp3wrap python3-tk python3-mutagen

# nautius reset
nautilus -q
echo ""
echo "You may look for install errors..."
echo ""
echo "finito"
exit
